import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { PokemonCatalogueComponent } from './pages/pokemon-catalogue/pokemon-catalogue.component';
import { PokemonCardComponent } from './components/pokemon-card/pokemon-card.component';
import { PokemonDetailsComponent } from './pages/pokemon-details/pokemon-details.component';
import { TrainerPageComponent } from './pages/trainer-page/trainer-page.component';
import { NavbarComponent } from './share/navbar/navbar.component';
import { PokeomnCardGridComponent } from './components/pokeomn-card-grid/pokeomn-card-grid.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    PokemonCatalogueComponent,
    PokemonCardComponent,
    PokemonDetailsComponent,
    TrainerPageComponent,
    NavbarComponent,
    PokeomnCardGridComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
