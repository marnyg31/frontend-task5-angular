import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon';

@Component({
  selector: 'app-pokeomn-card-grid',
  templateUrl: './pokeomn-card-grid.component.html',
  styleUrls: ['./pokeomn-card-grid.component.scss']
})
export class PokeomnCardGridComponent implements OnInit {
  @Input() pokemonList: Pokemon[] 

  constructor() { }

  ngOnInit(): void {
  }

}
