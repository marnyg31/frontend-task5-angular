import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokeomnCardGridComponent } from './pokeomn-card-grid.component';

describe('PokeomnCardGridComponent', () => {
  let component: PokeomnCardGridComponent;
  let fixture: ComponentFixture<PokeomnCardGridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokeomnCardGridComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokeomnCardGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
