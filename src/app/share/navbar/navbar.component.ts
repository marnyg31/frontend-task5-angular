import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  constructor(private router: Router, private authService: AuthService) { }
  onLogoutClick() {
    this.authService.logOut()
    this.router.navigateByUrl("/login")
  }

  ngOnInit(): void {
  }

}
