
export interface Pokemon {
    id: Number,

    name: string,
    types: Array<{slot:Number,type:{name: string, url: URL }}>,
    stats: Array<{ base_stat: Number, stat: { name: string, url: URL } }>,
    sprites: {
        front_default: URL,
        other: { 'official-artwork': { front_default: URL } }
    }

    height: Number,
    weight: Number,
    abilities: Array<{ ability: { name: string, url: URL }, is_hidden: boolean, slot: Number }>,
    base_experience: Number

    moves:Array<{move:{name:string, url:URL}}>
}
