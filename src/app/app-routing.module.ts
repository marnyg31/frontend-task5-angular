import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth/auth.guard';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { PokemonCatalogueComponent } from './pages/pokemon-catalogue/pokemon-catalogue.component';
import { PokemonDetailsComponent } from './pages/pokemon-details/pokemon-details.component';
import { TrainerPageComponent } from './pages/trainer-page/trainer-page.component';

const routes: Routes = [
  {
    path:"login",
    component: LandingPageComponent
  },
  {
    path:"trainer",
    component: TrainerPageComponent,
    canActivate:[AuthGuard] 
  },
  {
    path:"pokemon/:id",
    component: PokemonDetailsComponent
  },
  {
    path:"",
    component: PokemonCatalogueComponent,
    canActivate:[AuthGuard] 
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
