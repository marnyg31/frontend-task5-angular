import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon';
import { PokemonService } from 'src/app/services/pokemon/pokemon.service';
import { ProfileService } from 'src/app/services/profile.service';

@Component({
  selector: 'app-pokemon-details',
  templateUrl: './pokemon-details.component.html',
  styleUrls: ['./pokemon-details.component.scss']
})
export class PokemonDetailsComponent implements OnInit {
  public id: number;
  public pokemon: Pokemon

  constructor(private profileService:ProfileService,private route: ActivatedRoute, private pokemonService: PokemonService, private router :Router) { }
  
  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number

      this.pokemonService.getOnePokemon(this.id).subscribe(pokemon => {
        this.pokemon = pokemon
      })
    });
  }

  addToCurrentUser(){
    this.profileService.addPokemonToCurrentUser(this.pokemon)
    this.router.navigateByUrl("/trainer")
  }
}

