import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon';
import { ProfileService } from 'src/app/services/profile.service';

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer-page.component.html',
  styleUrls: ['./trainer-page.component.scss']
})
export class TrainerPageComponent implements OnInit {
  public user:string=localStorage.getItem("currentUser")

  public pokemonList:Pokemon[]
  constructor(private profileService:ProfileService) { }

  ngOnInit(): void {
    this.pokemonList=this.profileService.getAllPokemonForCurrentUser()

  }

}
