import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {
  // public name: string = ""
  public loginForm: FormGroup = new FormGroup({
    username: new FormControl("", [Validators.required,
    Validators.minLength(2)])
  })
  get username() {
    return this.loginForm.get("username")
  }

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
  }

  onNameSubmit(event: Event) {
    this.authService.logInn(this.loginForm.value.username)
    this.router.navigateByUrl("/")
  }

}
