import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon';
import { PokemonService } from 'src/app/services/pokemon/pokemon.service';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.component.html',
  styleUrls: ['./pokemon-catalogue.component.scss']
})
export class PokemonCatalogueComponent implements OnInit {
  public pokemonList: Pokemon[] = []


  constructor(private router: Router, private pokemonService: PokemonService, private route: ActivatedRoute) { }

  public indexFrom: number = 0
  public numPokemon: number = 10

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      if (params['numPokemon']) this.numPokemon = params["numPokemon"]
      if (params['from']) this.indexFrom = params["from"]
      this.getPokemon()
    });
  }

  getPokemon() {
    this.pokemonList = []
    this.pokemonService.getManyPokemon(this.indexFrom, this.numPokemon).subscribe((pk) => {
      this.pokemonList.push(pk)
    })
  }

  changePokemonSlice(sliceMovement: number) {
    this.indexFrom += sliceMovement
    if (this.indexFrom < 0) this.indexFrom = 0
    this.router.navigate(['/'], { queryParams: { indexFrom: this.indexFrom } });
  }
}
