import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Pokemon } from 'src/app/models/pokemon';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(private html: HttpClient) { }

  public getOnePokemon(id: number): Observable<Pokemon> {
    return this.html.get<Pokemon>("https://pokeapi.co/api/v2/pokemon/" + id)
  }

  public getManyPokemon(from:number=0,numPokemon:number=10): Observable<Pokemon> {

    return new Observable<Pokemon>(observer => {
      this.html.get<any>(`https://pokeapi.co/api/v2/pokemon?limit=${numPokemon}&offset=${from}`)
        .toPromise().then(res => {
          return res.results.map(r => r.url)
        }).then(urls => {
          urls.forEach((url) => {
            this.html.get<Pokemon>(url).subscribe(pokemon => {
              observer.next(pokemon);
            })
          })
        })
    });
  }
}
