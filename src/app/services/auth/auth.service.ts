import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  isLoggedIn(): boolean {
    return Boolean(localStorage.getItem("currentUser"))
  }
  logInn(name: string) {
    if (name) localStorage.setItem("currentUser", name)
  }
  logOut() {
    localStorage.removeItem("currentUser")
  }
}
