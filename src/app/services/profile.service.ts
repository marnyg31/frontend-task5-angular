import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor() { }

  getAllPokemonForCurrentUser() {
    let currentUser: string = localStorage.getItem("currentUser")
    return JSON.parse(localStorage.getItem(currentUser+"_pokemon")) || [];
  }
  addPokemonToCurrentUser(pokemon:Pokemon) {
    let currentUser: string = localStorage.getItem("currentUser")
    var allCapturedPokemon = JSON.parse(localStorage.getItem(currentUser+"_pokemon")) || [];
    allCapturedPokemon.push(pokemon); 
    localStorage.setItem(currentUser+"_pokemon",JSON.stringify(allCapturedPokemon))
  }
}
