# base image
FROM node:latest as build-step

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY package.json /app/package.json
RUN npm install
RUN npm install -g @angular/cli@7.3.9

# add app
COPY . /app

# start app
# ENTRYPOINT ng serve 
RUN npm run build --prod

FROM nginx:1.17.1-alpine
COPY --from=build-step /app/dist/pokemon /usr/share/nginx/html

