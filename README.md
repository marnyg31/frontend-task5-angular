# Pokemon

This repo contains a Angular app for managing pokemon's.

Upon first page load you will be prompted to enter a username,  you will then be redirected to the home page, which displays all the pokemon that you can add to your collection. To add a pokemon to your collection click on one of the pokemon cards presented, and oyu will be shown details about this specific pokemon. In this detail page you will find a capture button. When you capture pokemon, they will be stored based on your username and placed in the profile page. 

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
